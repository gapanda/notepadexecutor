﻿using System.Windows;
using Caliburn.Micro;
using NotepadExecutor.ViewModels;

namespace NotepadExecutor
{
    public class Bootstrapper : BootstrapperBase
    {
        public Bootstrapper()
        {
            Initialize();
        }

        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            DisplayRootViewFor<NotepadViewModel>();
        }
    }
}
