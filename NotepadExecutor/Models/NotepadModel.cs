﻿using NotepadExecutor.Helpers;
using System;
using System.Diagnostics;
using System.Linq;

namespace NotepadExecutor.Models
{
    public class NotepadModel
    {
        private ProcessInformation lpProcessInformation;

        public bool Start()
        {
            var lpStartupInfo = new StartUpInfo();
            lpProcessInformation = new ProcessInformation();

            return WinAPICallHelper.CreateProcess(WinAPICallHelper.NotepadPath, null, IntPtr.Zero, IntPtr.Zero,
                                                  false, 0, IntPtr.Zero, null, ref lpStartupInfo, out lpProcessInformation);
        }

        public void SetPositionAndResize(uint dwX, uint dwY, uint dwXSize, uint dwYSize)
        {
            var process = Process.GetProcessById(Convert.ToInt32(lpProcessInformation.dwProcessId));
            process.WaitForInputIdle();
            var hwnd = process.MainWindowHandle;
            WinAPICallHelper.SetWindowPos(hwnd, IntPtr.Zero, 
                                          Convert.ToInt32(dwX), 
                                          Convert.ToInt32(dwY), 
                                          Convert.ToInt32(dwXSize), 
                                          Convert.ToInt32(dwYSize), 
                                          WinAPICallHelper.SWP_SHOWWINDOW);
        }

        public bool Stop()
        {
            return WinAPICallHelper.TerminateProcess(lpProcessInformation.hProcess, 0);
        }
    }   
}
