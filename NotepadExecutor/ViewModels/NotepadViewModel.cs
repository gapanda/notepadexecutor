﻿using Caliburn.Micro;
using NotepadExecutor.Helpers;
using NotepadExecutor.Models;
using System;
using System.Windows;

namespace NotepadExecutor.ViewModels
{
    public class NotepadViewModel : Screen
    {
        public bool isRunning;

        private NotepadModel notepad;

        private string x;
        private string y;
        private string xSize;
        private string ySize;

        public string X
        {
            get
            {
                return x;
            }
            set
            {
                x = value;
                NotifyOfPropertyChange(() => X);
            }
        }
        public string Y
        {
            get
            {
                return y;
            }
            set
            {
                y = value;
                NotifyOfPropertyChange(() => Y);
            }
        }
        public string XSize
        {
            get
            {
                return xSize;
            }
            set
            {
                xSize = value;
                NotifyOfPropertyChange(() => XSize);
            }
        }
        public string YSize
        {
            get
            {
                return ySize;
            }
            set
            {
                ySize = value;
                NotifyOfPropertyChange(() => YSize);
            }
        }

        public void StartNotepad()
        {
            if(isRunning)
            {
                MessageBox.Show("Блокнот уже запущен!", "Ошибка!", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if(!PropertiesAreNullOrEmpty() && PropertiesAreUInt())
            {
                notepad = new NotepadModel();
                isRunning = notepad.Start();

                notepad.SetPositionAndResize(UInt32.Parse(X), UInt32.Parse(Y), UInt32.Parse(XSize), UInt32.Parse(YSize));

                if(!isRunning)
                {
                    notepad = null;
                }
            }
            else
            {
                MessageBox.Show("Введены неверные значения!", "Ошибка!", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
   
        }

        public void StopNotepad()
        {
            if(isRunning)
            {
                notepad.Stop();
                notepad = null;
                isRunning = false;
            }
        }

        private bool PropertiesAreNullOrEmpty()
        {
            if (!String.IsNullOrWhiteSpace(X) &&
                !String.IsNullOrWhiteSpace(Y) &&
                !String.IsNullOrWhiteSpace(XSize) &&
                !String.IsNullOrWhiteSpace(YSize))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private bool PropertiesAreUInt()
        {
            var xParseResult = UInt32.TryParse(X, out uint x);
            var yParseResult = UInt32.TryParse(Y, out uint y);
            var xSizeParseResult = UInt32.TryParse(XSize, out uint xSize);
            var ySizeParseResult = UInt32.TryParse(YSize, out uint ySize);

            if(xParseResult && yParseResult && xSizeParseResult && ySizeParseResult)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
